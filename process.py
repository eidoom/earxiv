#!/usr/bin/env python3

import pathlib, xml.etree.ElementTree, re, sqlite3, json, csv


def strip_whitespace(string):
    return re.sub(" *\n *", " ", string.strip())


if __name__ == "__main__":
    con = sqlite3.connect("arxiv_metadata.db")
    cur = con.cursor()

    with open("arxiv_metadata_schema.sql", "r") as f:
        cur.executescript(f.read())

    with open("categories.csv", "r") as f:
        all_cats = [(x[0], x[1]) for x in csv.reader(f)]

    cur.executemany(
        """
        INSERT INTO category (short_name, long_name)
        VALUES (?, ?)
        """,
        all_cats,
    )

    folder = pathlib.Path("data")

    a = 0

    # https://arxiv.org/archive/
    with open("subsumed.json", "r") as f:
        catsub = json.load(f)

    for file in folder.iterdir():
        with open(file, "r") as f:
            tree = xml.etree.ElementTree.parse(f)

        root = tree.getroot()

        list_records = root.find("{http://www.openarchives.org/OAI/2.0/}ListRecords")

        for record in list_records.findall(
            "{http://www.openarchives.org/OAI/2.0/}record"
        ):
            metadata = record.find("{http://www.openarchives.org/OAI/2.0/}metadata")

            try:
                arxiv = metadata.find("{http://arxiv.org/OAI/arXiv/}arXiv")
            except AttributeError:
                # this means record is deleted
                continue

            arxiv_id = arxiv.find("{http://arxiv.org/OAI/arXiv/}id").text

            created = arxiv.find("{http://arxiv.org/OAI/arXiv/}created").text

            title = strip_whitespace(
                arxiv.find("{http://arxiv.org/OAI/arXiv/}title").text
            )

            categories = arxiv.find(
                "{http://arxiv.org/OAI/arXiv/}categories"
            ).text.split()

            for i, cat in enumerate(categories):
                try:
                    categories[i] = catsub[cat]
                except KeyError:
                    pass

            primary_category = categories[0]

            abstract = strip_whitespace(
                arxiv.find("{http://arxiv.org/OAI/arXiv/}abstract").text
            )

            cur.execute(
                """
                INSERT INTO record (arxiv_id, created, title, abstract, primary_category_id)
                SELECT ?, unixepoch(?), ?, ?, id
                FROM category
                WHERE short_name = ?
                """,
                (arxiv_id, created, title, abstract, primary_category),
            )
            id = cur.lastrowid

            if a == id:
                # catch if category select fails
                print(id, arxiv_id, created, title, primary_category)
                exit()
            a = id

            cur.executemany(
                """
                INSERT INTO crosslist (record_id, category_id)
                SELECT ?, id
                FROM category
                WHERE short_name = ?
                """,
                [(id, x) for x in categories[1:]],
            )

            try:
                updated = arxiv.find("{http://arxiv.org/OAI/arXiv/}updated").text

                cur.execute(
                    """
                    INSERT INTO updated (id, data)
                    VALUES (?, unixepoch(?))
                    """,
                    (id, updated),
                )
            except AttributeError:
                pass

            try:
                comments = strip_whitespace(
                    arxiv.find("{http://arxiv.org/OAI/arXiv/}comments").text
                )

                cur.execute(
                    """
                    INSERT INTO comments (id, data)
                    VALUES (?, ?)
                    """,
                    (id, comments),
                )
            except AttributeError:
                pass

            # TODO add authors
            # for author in arxiv.find("{http://arxiv.org/OAI/arXiv/}authors").findall(
            #     "{http://arxiv.org/OAI/arXiv/}author"
            # ):
            #     keyname = author.find("{http://arxiv.org/OAI/arXiv/}keyname").text

            #     try:
            #         forenames = author.find(
            #             "{http://arxiv.org/OAI/arXiv/}forenames"
            #         ).text
            #     except AttributeError:
            #         pass

            #     try:
            #         suffix = author.find("{http://arxiv.org/OAI/arXiv/}suffix").text
            #     except AttributeError:
            #         pass

            #     try:
            #         affiliation = author.find(
            #             "{http://arxiv.org/OAI/arXiv/}affiliation"
            #         ).text
            #     except AttributeError:
            #         pass

    con.commit()
    con.close()
