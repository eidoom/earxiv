# [earxiv](https://gitlab.com/eidoom/earxiv)

**explore arxiv**

`harvest.py` can bulk download arxiv metadata using the [recommended](https://info.arxiv.org/help/bulk_data.html) [API](https://www.openarchives.org/OAI/2.0/openarchivesprotocol.htm).
However, this does not include citations.
For HEP, we have the [INSPIRE API](https://github.com/inspirehep/rest-api-doc).
There is also the [Semantic Scholar API](https://www.semanticscholar.org/product/api).

`process.py` can read the [`xml` data](https://arxiv.org/OAI/arXiv.xsd) downloaded and write it into an `sqlite3` database.

`analyse*.py` can query the database and plot some stuff.
