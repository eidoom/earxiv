#!/usr/bin/env python3

import time, math, pathlib, urllib.request, urllib.error, xml.etree.ElementTree

if __name__ == "__main__":
    folder = pathlib.Path("data")
    folder.mkdir(exist_ok=True)
    files = [*folder.iterdir()]
    if files:
        print("load last...")
        last = sorted(files, key=lambda p: int(p.stem))[-1]
        data = last.read_text()
    else:
        print("request first...")
        with urllib.request.urlopen(
            "https://export.arxiv.org/oai2?verb=ListRecords&metadataPrefix=arXiv&from=2021-01-01&until=2022-01-01"
        ) as f:
            data = f.read().decode()

    root = xml.etree.ElementTree.fromstring(data)
    list_records = root.find("{http://www.openarchives.org/OAI/2.0/}ListRecords")
    resumption_token_element = list_records.find(
        "{http://www.openarchives.org/OAI/2.0/}resumptionToken"
    )
    cursor = int(resumption_token_element.get("cursor"))
    total = int(resumption_token_element.get("completeListSize"))
    page = len([*list_records.findall("{http://www.openarchives.org/OAI/2.0/}record")])
    pages = math.ceil(total / page)
    print(f"{cursor} to {cursor+page} ({page} per page) of {total} (page {cursor//page} of {pages})")

    if not files:
        print(f"write...")
        with open(f"data/{cursor}.xml", "w") as f:
            f.write(data)

    resumption_token = resumption_token_element.text

    while resumption_token:
        try:
            print("request subsequent...")
            with urllib.request.urlopen(
                f"https://export.arxiv.org/oai2?verb=ListRecords&resumptionToken={resumption_token}"
            ) as f:
                data = f.read().decode()

            root = xml.etree.ElementTree.fromstring(data)
            list_records = root.find(
                "{http://www.openarchives.org/OAI/2.0/}ListRecords"
            )
            resumption_token_element = list_records.find(
                "{http://www.openarchives.org/OAI/2.0/}resumptionToken"
            )
            cursor = int(resumption_token_element.get("cursor"))

            print(f"write {100*cursor/page/pages:.1f}%...")
            with open(f"data/{cursor}.xml", "w") as f:
                f.write(data)

            resumption_token = resumption_token_element.text

        except urllib.error.HTTPError as e:
            wait = int(e.getheader("Retry-After"))
            print(f"waiting {wait}s...")
            time.sleep(wait)
