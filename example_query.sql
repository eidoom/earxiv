SELECT c.short_name, c.long_name, COUNT(r.id)
FROM record AS r
INNER JOIN category AS c ON c.id = r.primary_category_id
WHERE r.created > unixepoch('now', '-1 year')
GROUP BY c.id
ORDER BY COUNT(r.id);
