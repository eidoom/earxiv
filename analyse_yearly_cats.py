#!/usr/bin/env python3

import sqlite3, collections
import matplotlib.pyplot


if __name__ == "__main__":
    con = sqlite3.connect("arxiv_metadata.db")
    cur = con.cursor()

    (final_year,) = cur.execute(
        """
        SELECT UNIXEPOCH(created, 'unixepoch', 'start of year')
        FROM record
        ORDER BY created DESC
        LIMIT 1
        """
    ).fetchone()

    res = cur.execute(
        """
        WITH totals AS (
            SELECT c.id FROM category AS c
            INNER JOIN record AS r ON c.id = r.primary_category_id
            WHERE
                r.created < :final_year
                AND r.created > UNIXEPOCH(:final_year, 'unixepoch', '-1 years')
            GROUP BY c.id
            ORDER BY COUNT(r.id) DESC
            LIMIT :num_lines
        )
        SELECT
            c.long_name || ' (' || c.short_name || ')',
            CAST(SUBSTR(DATE(created, 'unixepoch'), 0, 5) AS INTEGER) AS year,
            COUNT(r.id) AS count
        FROM category AS c
        INNER JOIN record AS r ON c.id = r.primary_category_id
        WHERE
            c.id IN totals
            AND r.created < :final_year
        GROUP BY
            c.id,
            year
        """,
        {
            "num_lines": 10,
            "final_year": final_year,
        },
    ).fetchall()

    con.close()

    data = collections.defaultdict(dict)
    for cat, year, count in res:
        data[year][cat] = count

    years = sorted(data.keys())

    cats = sorted(
        set([x[0] for x in res]),
        key=lambda x: d[x] if x in (d := data[years[-1]]) else 0,
        reverse=True,
    )

    fig, ax = matplotlib.pyplot.subplots()

    ax.set_title(
        "arXiv preprint submission history of last year's ten most active archives"
    )
    ax.set_xlabel("Year")
    ax.set_ylabel("Preprints per archive per year")

    for cat in cats:
        ax.plot(
            years, [data[y][cat] if cat in data[y] else None for y in years], label=cat
        )

    ax.legend()

    fig.savefig("yearly_cats.pdf", bbox_inches="tight")
