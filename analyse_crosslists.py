#!/usr/bin/env python3

import sqlite3
import matplotlib.pyplot, matplotlib.patches


def pluck_factory(_, row):
    return row[0]


if __name__ == "__main__":
    con = sqlite3.connect("arxiv_metadata.db")
    cur = con.cursor()

    cur.row_factory = None
    res = cur.execute(
        """
        SELECT
            c.short_name,
            x.short_name,
            COUNT(r.id) AS count
        FROM record AS r
        INNER JOIN category AS c ON c.id = r.primary_category_id
        INNER JOIN crosslist AS xl ON xl.record_id = r.id
        INNER JOIN category AS x ON x.id = xl.category_id
        GROUP BY c.id, x.id
        HAVING count > 4000
        ORDER BY count DESC
        """,
    ).fetchall()

    con.close()

    l = 6.4
    r = 100
    m = max(x[2] for x in res)

    x = [x[0] for x in res]
    y = [x[1] for x in res]
    c = [r * l * x[2] / m for x in res]

    fig, ax = matplotlib.pyplot.subplots(figsize=(l, l))

    ax.set_aspect("equal")

    ax.tick_params(axis="x", labelrotation=90)

    ax.scatter(x, y, c, marker="o", alpha=0.5, linewidths=0)

    ax.set_title("Correlations of archive categories by crosslistings")
    ax.set_xlabel("Primary category")
    ax.set_ylabel("Crosslisted category")

    fig.savefig("crosslists.pdf", bbox_inches="tight")
